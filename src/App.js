import React, { Component } from 'react';
import './App.css';
import Timeline from './Timeline';
import dummyData from './dummyData';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Timeline data={dummyData} />
      </div>
    );
  }
}

export default App;
