import React from 'react'
import TimelineElement from './TimelineElement';
import './Timeline.css';

const Timeline = ({data}) => {
    return (
        <div className="timeline-container">
            {data.map((elem, idx) => <TimelineElement data={elem} key={idx}></TimelineElement>)}
        </div>
    )
};

export default Timeline;
