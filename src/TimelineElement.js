import React from 'react'

const TimelineElement = ({data}) => {
    let link = null;
    if (!!data.link) {
        link = <p className="link"><a href={data.link.url}>{data.link.text} ></a></p>
    }
    return (
            <div className="timeline-element">
                <div className="timeline-element-content">
                    <span className="label" style={{backgroundColor: data.category.color}}>{data.category.text}</span>
                    <div className="text">
                        <p className="date">{data.date}</p>
                        <p className="title">{data.text}</p>
                        {link}
                    </div>
                    <span className="circle" />
                </div>
            </div>
    )
};

export default TimelineElement;
